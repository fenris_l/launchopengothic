use std::process::Command;

use ashpd::{
    desktop::file_chooser::{FileChooserProxy, OpenFileOptions},
    zbus, WindowIdentifier,
};

const PROGRAM_NAME: &str = "Launch OpenGothic";

#[tokio::main(flavor = "current_thread")]
async fn main() -> ashpd::Result<()> {
    let connection = zbus::Connection::session().await?;
    let proxy = FileChooserProxy::new(&connection).await?;
    let window_identifier = WindowIdentifier::default();
    let open_file_options = OpenFileOptions::default().directory(true);

    let opened_file = proxy
        .open_file(
            &window_identifier,
            &format!("{} - Pick the directory containing Gothic", PROGRAM_NAME),
            open_file_options,
        )
        .await?;

    // Convert URI to path
    let gothic_path_url = opened_file.uris()[0].strip_prefix("file://").unwrap();
    
    // This assumes Gothic2Notr is available in PATH
    Command::new("Gothic2Notr")
        .arg("-g")
        .arg(gothic_path_url)
        .spawn()
        .expect("Failed to open OpenGothic");

    Ok(())
}
